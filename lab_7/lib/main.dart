import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lab_7/input_pair.dart';

void main() {
  runApp(
    const MaterialApp(
      home: MyApp()
    )
  );
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<void> createAppointment(BuildContext context) async {
    return await showDialog(context: context,
      builder: (context) {
        return AlertDialog(
          backgroundColor: const Color(0xff1b1d24),
          content: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Title:",
                    style: GoogleFonts.mPlusRounded1c(
                        fontSize: 20, color: Colors.white),
                  ),
                  TextFormField(
                    style: const TextStyle(fontSize: 14, color: Colors.white),
                    validator: (value) {
                      return value!.isNotEmpty ? null : "Please fill this field!";
                    },
                    decoration: InputDecoration(
                      isDense: true,
                      hintText: "Appointment Title",
                      hintStyle: const TextStyle(color: Colors.white54),
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: double.infinity,
                    height: 10,
                  ),
                  Text(
                    "Attendees:",
                    style: GoogleFonts.mPlusRounded1c(
                        fontSize: 20, color: Colors.white),
                  ),
                  TextFormField(
                    style: const TextStyle(fontSize: 14, color: Colors.white),
                    validator: (value) {
                      return value!.isNotEmpty ? null : "Please fill this field!";
                    },
                    minLines: 1,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                      isDense: true,
                      hintText: "Comma separated username",
                      hintStyle: const TextStyle(color: Colors.white54),
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: double.infinity,
                    height: 10,
                  ),
                  Text(
                    "Date:",
                    style: GoogleFonts.mPlusRounded1c(
                        fontSize: 20, color: Colors.white),
                  ),
                  TextFormField(
                    style: const TextStyle(fontSize: 14, color: Colors.white),
                    validator: (value) {
                      return value!.isNotEmpty ? null : "Please fill this field!";
                    },
                    decoration: InputDecoration(
                      isDense: true,
                      hintText: "Date",
                      hintStyle: const TextStyle(color: Colors.white54),
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: double.infinity,
                    height: 10,
                  ),
                  Text(
                    "Location:",
                    style: GoogleFonts.mPlusRounded1c(
                        fontSize: 20, color: Colors.white),
                  ),
                  TextFormField(
                    style: const TextStyle(fontSize: 14, color: Colors.white),
                    validator: (value) {
                      return value!.isNotEmpty ? null : "Please fill this field!";
                    },
                    decoration: InputDecoration(
                      isDense: true,
                      hintText: "Put \"-\" to leave it blank",
                      hintStyle: const TextStyle(color: Colors.white54),
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: double.infinity,
                    height: 10,
                  ),
                  Text(
                    "Link (Optional):",
                    style: GoogleFonts.mPlusRounded1c(
                        fontSize: 20, color: Colors.white),
                  ),
                  TextFormField(
                    style: const TextStyle(fontSize: 14, color: Colors.white),
                    validator: (value) {
                      return value!.isNotEmpty ? null : "Please fill this field!";
                    },
                    decoration: InputDecoration(
                      isDense: true,
                      hintText: "Put \"-\" to leave it blank",
                      hintStyle: const TextStyle(color: Colors.white54),
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: double.infinity,
                    height: 10,
                  ),
                  Text(
                    "Description:",
                    style: GoogleFonts.mPlusRounded1c(
                        fontSize: 20, color: Colors.white),

                  ),
                  TextFormField(
                    style: const TextStyle(fontSize: 14, color: Colors.white),
                    validator: (value) {
                      return value!.isNotEmpty ? null : "Please fill this field!";
                    },
                    minLines: 3,
                    maxLines: null,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                      isDense: true,
                      hintText: "Description",
                      hintStyle: const TextStyle(color: Colors.white54),
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.white),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: double.infinity,
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
          title: const Text("New Appointment", style: TextStyle(color: Colors.white),),
          actions: <Widget>[
            TextButton(
                onPressed: () {
                  if(_formKey.currentState!.validate()) {
                    Navigator.of(context).pop();
                  }
                },
                child: const Text("Create"))
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Siplash',
      theme: ThemeData(scaffoldBackgroundColor: const Color(0xff1b1d24)),
      home: Scaffold(
          appBar: AppBar(title: const Text("SIPLASH")),
          floatingActionButton: FloatingActionButton(
            onPressed: () async {
              await createAppointment(context);
            },
            child: const Icon(Icons.add),
          ),
          body: Center(
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.all(20),
                  child: Text(
                    "My Appointments",
                    style: GoogleFonts.mPlusRounded1c(
                        fontSize: 32, color: Colors.white),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "This week",
                        textAlign: TextAlign.left,
                        style: GoogleFonts.mPlusRounded1c(
                            fontSize: 24, color: Colors.redAccent),
                      ),
                      const Divider(
                        color: Colors.white,
                      )
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 10, 20, 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Upcoming",
                        textAlign: TextAlign.left,
                        style: GoogleFonts.mPlusRounded1c(
                            fontSize: 24, color: Colors.yellowAccent),
                      ),
                      const Divider(
                        color: Colors.white,
                      )
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 10, 20, 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Done",
                        textAlign: TextAlign.left,
                        style: GoogleFonts.mPlusRounded1c(
                            fontSize: 24, color: Colors.green),
                      ),
                      const Divider(
                        color: Colors.white,
                      )
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
