import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TextAndInput extends StatelessWidget {

  final TextEditingController tec;
  final String teks;

  const TextAndInput(this.tec, this.teks, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
        children: <Widget>[
          Column(
            children: [
              Text(
                "Title:",
                style: GoogleFonts.mPlusRounded1c(
                    fontSize: 20, color: Colors.black),
              ),
              TextFormField(
                controller: tec,
                validator: (value) {
                  return value!.isNotEmpty ? null : "Please fill this field!";
                },
                decoration: InputDecoration(
                  hintText: teks,
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(width: 1, color: Colors.black),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(width: 1, color: Colors.black),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: const BorderSide(width: 1, color: Colors.black),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: const BorderSide(width: 1, color: Colors.black),
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
              const SizedBox(
                width: double.infinity,
                height: 10,
              ),
            ],
          ),

        ]
    );
  }
}
