from .views import index,add_note,note_list
from django.urls import path

urlpatterns = [
    path('', index, name='index'),
    path('home', index, name='index'),
    path('add-note', add_note, name='add-note'),
    path('note-list', note_list, name='note_list-note'),
]