from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ('to','From','title','message')
        widgets = {
            'to' : forms.TextInput(attrs={'class':'form-control'}),
            'From' : forms.TextInput(attrs={'class':'form-control'}),
            'title' : forms.TextInput(attrs={'class':'form-control'}),
            'message' : forms.Textarea(attrs={'class':'form-control'}),
        }
        
