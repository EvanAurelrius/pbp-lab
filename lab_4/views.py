from django.shortcuts import render,redirect
from lab_2.models import Note
from .forms import NoteForm

def index(request):
    notes = Note.objects.all
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm
    if request.method == 'POST':
        noteForm = NoteForm(request.POST)
        if noteForm.is_valid():
            noteForm.save()
            return redirect('/lab-4')
    return render(request, 'lab4_form.html', {'form':form})

def note_list(request):
    notes = Note.objects.all
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)