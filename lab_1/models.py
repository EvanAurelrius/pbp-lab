from django.db import models

# Create Friend model that contains name, npm, and DOB (date of birth) here
class Friend(models.Model):
    name = models.CharField(max_length=30)
    # Implement missing attributes in Friend model
    npm = models.IntegerField()
    DOB = models.DateField()
