from django.contrib import admin

# Register Friend model
from .models import Friend
admin.site.register(Friend)