# Generated by Django 3.2.7 on 2021-09-19 08:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0002_alter_friend_npm'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friend',
            name='DOB',
            field=models.DateField(),
        ),
    ]
