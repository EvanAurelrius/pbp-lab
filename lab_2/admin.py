from django.contrib import admin

# Register Note model
from .models import Note
admin.site.register(Note)