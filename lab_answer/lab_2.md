1. Apakah perbedaan antara JSON dan XML?
> JSON lebih simple dan mudah dibaca daripada XML, tetapi penggunaan XML untuk pertukaran data yang kompleks jauh lebih baik dibandingkan dengan JSON.
> Objek pada JSON memiliki tipe, sedangkan pada XML datanya tidak memiliki tipe.
> JSON mendukung beberapa tipe data seperti string, number, array, dan Boolean, sedangkan pada XML, keseluruhan data bertipe string.
> Tampilan sintaks pada JSON tidak bisa diubah, sedangkan XML bisa karena merupakan XML merupakan sebuah markup language. 
> Pada JSON kita tidak dapat menambahkan komentar, sedangkan pada XML kita bisa menambahkan komentar.
> JSON hanya mendukung UTF-8 encoding, sedangkan XML mendukung beberapa macam encoding format.
> XML menggunakan tag dalam menampilkan representasi datanya sehingga lebih machine-readable dibandingkan dengan JSON.
> JSON didukung oleh hampir seluruh browser, sedangkan browser yang mendukung XML jumlahnya lebih terbatas. 

2. Apakah perbedaan antara HTML dan XML?
> XML berfokus pada transfer data, sedangkan HTML berfokus pada UI (User Interface).
> XML case sensitive, HTML tidak.
> XML supprort namespace, HTML tidak.
> XML harus terdapat closing tag, HTML tidak.
> Tag XML dapat dikembangkan sedangkan HTML memiliki tag limited.