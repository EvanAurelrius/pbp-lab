import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Siplash',
      theme: ThemeData(scaffoldBackgroundColor: const Color(0xff1b1d24)),
      home: const MyHomePage(title: 'SIPLASH'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
                decoration: const BoxDecoration(
                  color: Colors.blue,
                ),
                child: Text(
                  "SIPLASH",
                  style: GoogleFonts.mPlusRounded1c(fontSize: 32),
                ),
            ),
            ListTile(
              title: const Text('Login'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('Sign Up'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: const Text('About'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: Center(
        child: Column(
          children: [
            Container(
                margin: const EdgeInsets.fromLTRB(0, 120, 0, 50),
                height: 260,
                width: 260,
                child: const Image(image: AssetImage('assets/logo.png'))),
            const Text(
              "SIPLASH",
              style: TextStyle(color: Colors.white, fontSize: 30),
            ),
            const Text(
              "Simple Planner for Staying at Home",
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: const EdgeInsets.fromLTRB(0, 20, 20, 0),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(
                          msg: "You should be redirected to Our Login Page",
                          toastLength: Toast.LENGTH_SHORT,
                          timeInSecForIosWeb: 1,
                          fontSize: 16.0);
                    },
                    child: const Text("Login"),
                    style: ElevatedButton.styleFrom(
                        fixedSize: const Size(100, 30),
                        primary: const Color(0xFF3465a4)),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(20, 20, 0, 0),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(
                          msg: "You should be redirected to Our Sign Up Page",
                          toastLength: Toast.LENGTH_SHORT,
                          timeInSecForIosWeb: 1,
                          fontSize: 16.0);
                    },
                    child: const Text("Sign Up"),
                    style: ElevatedButton.styleFrom(
                        fixedSize: const Size(100, 30),
                        primary: const Color(0xFF3465a4)),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
