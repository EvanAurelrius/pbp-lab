from django.shortcuts import render
from django.shortcuts import redirect
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.http import JsonResponse

def index(request):
    return render(request, 'lab5_index.html')

def getNotes(request):
    notes = Note.objects.all()
    return JsonResponse({'notes':list(notes.values())})